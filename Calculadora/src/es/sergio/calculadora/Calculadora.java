package es.sergio.calculadora;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Calculadora
 */
@WebServlet("/calculadora")
public class Calculadora extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Calculadora() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int num1 = Integer.valueOf(request.getParameter("num1"));
		int num2 = Integer.valueOf(request.getParameter("num2"));
		String ope = request.getParameter("operacion");
		response.setContentType("text/html");
		int resultado = 0;
		
		switch (ope) {
		case "suma":
			resultado = num1 + num2;
			break;
		case "resta":
			resultado = num1 - num2;
			break;
		case "mult":
			resultado = num1 * num2;
			break;
		case "div":
			resultado = num1 / num2;
			break;
		}
		
		response.getWriter().write("<h1>El resultado es: " + resultado + "</h1>");
		
	}

}
